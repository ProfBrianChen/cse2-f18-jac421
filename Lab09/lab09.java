// Name: Jenna Catalano 
// Date: 11/16/2018
// Course: CSE 002 Section 110
//
// Creates an array with 8 values and can invert, copy , or invert those using 4 different methods.
//
//initialize the public class as lab09
import java.util.Arrays;
public class lab09{
  //main method used for all java code
  public static void main(String[]args){
    int [] array0 = {1,2,3,4,5,6,7,8}; //initiallize an array
    int [] array1 = copy(array0); //array1 is a copy of array0
    int [] array2 = copy(array0); //array2 is also a copy of array0
    inverter(array0); //invert array0
    print(array0);//print inverted array
    print(inverter2(array1)); //invert and print array1 using inverter2 method
    int[]array3 = (inverter2(array2));//intiallize array3 as the inversion of array 2
    print(array3);
  }
  //method copies the array inputted and returns it
  public static int[]copy(int[]array1){
    int[]array = new int[array1.length];
    for(int i = 0; i < array.length; i++){
      array[i] = array1[i];
    }
    return array;
  }
  //method inverts an array and returns void
  public static void inverter(int[]array1){
    int[]array = copy(array1);
    for(int i = 0; i < array1.length; i++){
      array1[i] = array[array1.length - (i + 1)];
    }
    return;
  }
  //method inverts inputted array and returns that array
  public static int[] inverter2(int[]array1){
    int[]array = copy(array1);
    for(int i = 0; i < array.length; i++){
      array[i] = array1[array.length -(1 + i)];
    }
    return array;
    }
  //method prints the array with a space after
  public static void print(int[]array1){
    for(int i =0; i < array1.length; i++){
      System.out.print(array1[i]);
    }
    System.out.println();
    return;
  }
   
   
    
    
}

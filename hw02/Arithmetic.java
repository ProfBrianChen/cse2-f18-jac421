//Jenna Catalano
//9/11/2018
//Section: 110
//CSE 02 Arithmetic
//
//This program will calculate the cost of different items you bought at the store including the PA sales tax of 6%
//
public class Arithmetic{
  //method used for program
  public static void main (String[]args){
    
    int numPants      = 3;     //Number of pairs of pants
    double pantsPrice = 34.98; //Cost per pair of pants
    int numShirts     = 2;     //Number of sweatshirts
    double shirtPrice = 24.99; //Cost per shirt
    int numBelts      = 1;     //Number of belts
    double beltCost   = 33.99; //cost per belt
    double paSalesTax = 0.06;  //the tax rate
    
    double totalPantsPrice  = numPants * pantsPrice;  //total cost for pants
    double totalShirtsPrice = numShirts * shirtPrice; //total cost for shirts
    double totalBeltsPrice  = numBelts * beltCost;    //total cost for belts
    
    double pantsSalesTax    = (int)((paSalesTax * totalPantsPrice) * 100) / 100.0;  //total sales tax for pants
    double shirtsSalesTax   = (int)((paSalesTax * totalShirtsPrice) * 100) / 100.0; //total sales tax for shirts
    double beltsSalesTax    = (int)((paSalesTax * totalBeltsPrice) * 100) / 100.0;  //total sales tax for belts
    
    double totalCostOfPurchases = (totalPantsPrice + totalShirtsPrice + totalBeltsPrice); //total cost of purchases without tax
    double totalSalesTax = (pantsSalesTax + shirtsSalesTax + beltsSalesTax); //total sales tax on purchases
    double totalPaid = (totalSalesTax + totalCostOfPurchases); //total paid for transaction including sales tax
    
    
    System.out.println("The total price for pants is $" + totalPantsPrice); //prints the total price of pants
    System.out.println("The total price for shirts is $" + totalShirtsPrice); //prints the total price of shirts
    System.out.println("The total price for belts is $" + totalBeltsPrice); //prints the total price of belts
    System.out.println("The total sales tax for pants is $" + pantsSalesTax); //print the total sales tax for pants
    System.out.println("The total sales tax for shirts is $" + shirtsSalesTax); //print the total sales tax for shirts
    System.out.println("The total sales tax for belts is $" + beltsSalesTax); //print the total sales tax for belts
    System.out.println("The total cost of your purchases without tax is $" + totalCostOfPurchases); //print the total cost of purchases without tax
    System.out.println("The total sales tax on your purchases is $" + totalSalesTax); //print the total sales tax on purchases
    System.out.println("The total paid for this transaction including sales tax is $" + totalPaid); //print the total paid for transaction including sales tax
    

  }
}
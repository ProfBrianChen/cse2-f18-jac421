// Name: Jenna Catalano 
// Date: 10/4/2018
// Course: CSE 002 Section 110
//
// This program will prompt the user for course number, department name, times in a week, time of day, instructor name, and number of students in the class
// and will determine if the user inputted acceptable values and print either an error message or the values entered.
//
import java.util.Scanner;

//initialize public class WhileLoop
public class UserInput{
  //the main method used for every java program
  public static void main (String[]args){
    
    Scanner scnr = new Scanner(System.in);
    
 
    int courseNumber = 0;
   
    
    do{
      System.out.println("Enter an integer value for course number.");
      boolean correctInt = scnr.hasNextInt();
      if(correctInt){
        courseNumber = scnr.nextInt();
      }
      else{
        System.out.println("You did not enter a valid course number.");
        scnr.next();
      }
    } while(courseNumber == 0);
    
    System.out.println("The course number is " + courseNumber);
    

     String departmentName = "No";
    
     do{
       System.out.println("Enter a string value for department name.");
       boolean correctString = scnr.hasNext();
       if(correctString){
         departmentName = scnr.next();
       }
       else{
         System.out.println("You did not enter a valid department name.");
         scnr.next();
       }
     } while(departmentName.equals("No"));
    
    System.out.println("The department name is " + departmentName );
    
    
     int timesInAWeek = 0;
    
    do{
      System.out.println("Enter an integer value for times in a week.");
      boolean correctInt1 = scnr.hasNextInt();
      if(correctInt1){
        timesInAWeek = scnr.nextInt();
      }
      else{
        System.out.println("You did not enter a valid number of times in a week.");
        scnr.next();
      }
    } while(timesInAWeek == 0);
    
    System.out.println("The times in a week is " + timesInAWeek);
    
      String startingTime = "0:00";
    
    do{
      System.out.println("Enter a string value for starting time.");
      boolean correctString1 = scnr.hasNext();
      if(correctString1){
        startingTime = scnr.next();
      }
      else{
        System.out.println("You did not enter a valid starting time.");
        scnr.next();
      }
    } while(startingTime.equals("0:00"));
    
    System.out.println("The starting time is " + startingTime);
    
    String teacherName = "Bob";
    
    do{
      System.out.println("Enter a string value for the teacher's name.");
      boolean correctString2 = scnr.hasNext();
      if(correctString2){
        teacherName = scnr.next();
      }
      else{
        System.out.println("You did not enter a valid teacher name.");
        scnr.next();
      }
    } while(teacherName.equals("Bob"));
    
    System.out.println("The teacher's name is " + teacherName);
    
     int numberOfStudents = 0 ;
    
    do{
      System.out.println("Enter an integer value for number of students");
      boolean correctInt2 = scnr.hasNextInt();
      if(correctInt2){
        numberOfStudents = scnr.nextInt();
      }
      else{
        System.out.println("You did not enter a valid number of students.");
        scnr.next();
      }
    } while(numberOfStudents == 0);
    
    System.out.println("The number of students is " + numberOfStudents);
    
  }
}
// Name: Jenna Catalano 
// Date: 11/27/2018
// Course: CSE 002 Section 110
//
// Creates an array of 10 random integers 0-9 then removes the position and value of your choice.
//
import java.util.Scanner;
public class RemoveElements{
  public static void main(String [] arg){
 Scanner scan=new Scanner(System.in);
int num[]=new int[10];
int newArray1[];
int newArray2[];
int index,target;
 String answer="";
 do{
   System.out.print("Random input 10 ints [0-9]");
   num = randomInput();
   String out = "The original array is:";
   out += listArray(num);
   System.out.println(out);
 
   System.out.print("Enter the index ");
   index = scan.nextInt();
   newArray1 = delete(num,index);
   String out1="The output array is ";
   out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
   System.out.println(out1);
 
      System.out.print("Enter the target value ");
   target = scan.nextInt();
   newArray2 = remove(num,target);
   String out2="The output array is ";
   out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
   System.out.println(out2);
    
   System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
   answer=scan.next();
 }while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){
 String out="{";
 for(int j=0;j<num.length;j++){
   if(j>0){
     out+=", ";
   }
   out+=num[j];
 }
 out+="} ";
 return out;
  }
  //creates and returns an array of random numbers from 0-9
  public static int [] randomInput(){
    int array[] = new int [10];
    for(int i = 0; i < array.length; i++){
      int x = ((int)(Math.random() * (array.length - 1)) + 1);
      array[i] = x;
    }
    return array;
  }
  //deletes the value at a given position and returns an array without this value that is one shorter in length
  public static int[] delete(int[]list, int pos){
     Scanner scan = new Scanner(System.in);
     int array[] = new int[list.length-1];
    int n = 0;
    while(pos > 9 || pos < 0){
      System.out.println("Position invalid, please enter another.");
      pos = scan.nextInt();
    }
    list[pos] = -1;
    for(int i = 0; i < list.length; i++){
      if(list[i] != -1){
        array[n] = list[i];
        n++;
      }
    }
    
  return array;
  }
  //finds how many times a target value is found in the array and removes it and returns an array without it.
 public static int[] remove(int[]list, int target){
   int m = 0;
   int n = 0;
   for(int i = 0; i < list.length; i++){
     if(target == list[i]){
       list[i] = -1;
       m++;
     }
   }
   int array[] = new int[list.length - (1+m)];
    for(int i = 0; i < list.length; i++){
      if(list[i] != -1){
        array[n] = list[i];
        n++;
      }
    }  
     return array;   
}
}
 

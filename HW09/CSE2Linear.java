// Name: Jenna Catalano 
// Date: 11/27/2018
// Course: CSE 002 Section 110
//
// Creates an array of 15 final grade and then do a search for a specific grade, then scramble, then do another search
//
//initialize the public class as CSE2Linear
import java.util.Arrays;
import java.util.Scanner;
import java.util.Random;
public class CSE2Linear{
   //main method used for all java code
  public static void main(String[]args){
    int [] array = new int [15];//new array with 15 spots
    Scanner scnr = new Scanner(System.in);//new Scanner scnr
    int x = 0; 
    //fill an array with 15 integers if the entered values are integers 1-100 and are in ascending order
    for(int i = 0; i < 15; i++){
      do{
      System.out.println("Please enter an integer final grade from 1-100");
      boolean correctInt = scnr.hasNextInt();
      if(!correctInt){
        System.out.println("You didn't enter an integer, silly!");
      }
      else{
        x = scnr.nextInt();
        if(x > 100 || x < 1){
          System.out.println("Thats not from 1 - 100, silly");
        }
        else{
          if(i>0){
            if( x < array[i - 1]){
              System.out.println("Thats not greater or equal to your last input, silly!");
            }
            else{
              array[i] = x;
            }
          }
         else{
          array[i] = x;
            }
        }
      }
      }while(array[i] == 0);
    }
    System.out.println(Arrays.toString(array));//print initial array
    //have user input a grade to be found
    System.out.println("Please enter a grade to be searched for");
    int grade = scnr.nextInt();
    binarySearch(array, grade);//search for grade in array of grades
    //print scrambled array
    System.out.println("Scrambled:");
    randomScrambling(array);
    System.out.println(Arrays.toString(array));
    //have user input a grade to be found
    System.out.println("Please enter another grade to be searched for");
    int grade1 = scnr.nextInt();
    linearSearch(array, grade1);//search for grade in scrambled array of grades
    
  }
  //do a binary search on the array for a given grade and print whether it was found and the amount of iterations
  public static void binarySearch(int[]array, int x){
    int m = 0;
    int low = 0;
  int high = array.length - 1;
  while(high >= low) {
    ++m;
   int mid = (low + high)/2;
   if (x < array[mid]) {
    high = mid - 1;
   }
   if (x == array[mid]) {
     System.out.println("The grade was found with " + m + " iterations");
    return ;
   }
   if(x > array[mid]) {
    low = mid + 1;
   }
  }
  System.out.println("The grade was not found, there were " + m + " iterations");
return;
  }
  //randomly scramble the array
  public static void randomScrambling(int[]array){
     for(int i=0; i < 100; i++){
    int n = array[0];
    int l = ((int)(Math.random() * (array.length - 1)) + 1);
    int m = array[l];
    array[l] = n;
    array[0] = m;
  }
     return;
  }
  //search for a grade in the array using linear search and print whether it was found and amount of iterations
  public static void linearSearch(int[]array, int x){
    int n = 0;
    for ( int i = 0; i < array.length; i++){
      ++n;
      if( x == array[i]){
        System.out.println("The grade was found with " + n + " iterations");
        return;
      }
    }
      System.out.println("The grade was not found with " + n + " iterations");
      return;
  }
}
          


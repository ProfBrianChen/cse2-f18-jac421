// Name: Jenna Catalano 
// Date: 10/9/2018
// Course: CSE 002 Section 110
//
// This program will calculate the amount of one pairs, two pairs, 3 of a kind, and 4 of a kinds in randomly selected 
// hands and calculate the probability for each and the total number of loops.
//
import java.util.Scanner;

//initialize the public class as hw05
public class Hw05{
  
  //main method used for all java code
  public static void main (String[]args){
    
   
    Scanner scnr = new Scanner(System.in); //initialize the scanner scnr
    
    System.out.println("Enter a number of hands"); //ask the user for a number of hands
    int numHands = scnr.nextInt(); //numHands equals the integer the user enters
    
    int onePair = 0; //initialize onePair as 0
    int twoPair = 0; //initialize twoPair as 0
    int threeOfAKind = 0; //initialize threeOfAKind as 0
    int fourOfAKind = 0; //initialize fourOfAKind as 0
    int loopCount = 0; //initialize loopCount as 0
    
    //enter the loop and continue until numHands equals zero
      do{
        //initiallize cards 1-5 as random numbers 1-52
      int card1 = (int)(Math.random() * ((52 - 1) + 1)) + 1;
      int card2 = (int)(Math.random() * ((52 - 1) + 1)) + 1;
      int card3 = (int)(Math.random() * ((52 - 1) + 1)) + 1;
      int card4 = (int)(Math.random() * ((52 - 1) + 1)) + 1;
      int card5 = (int)(Math.random() * ((52 - 1) + 1)) + 1;
      //use remainders to find cards that are the same face
      card1 = card1 % 13;
      card2 = card2 % 13;
      card3 = card3 % 13;
      card4 = card4 % 13;
      card5 = card5 % 13;
      //if there are four cards of the same suit increment four of a kind
      if(((card1 == card2) && (card2 == card3) && (card3 == card4)) || ((card2 == card3) && (card3 == card4) && (card4 == card5)) || ((card1 == card3)&& (card3 == card4) && (card4 == card5)) ||
         ((card1 == card2) && (card2 == card4) && (card4 == card5)) || ((card1 == card2) && (card2 == card3) && (card3 == card5))){
        fourOfAKind = fourOfAKind + 1;
      }
      //if there are three cards of the same suit increment three of a kind
      if(((card1 == card2)&& (card2 == card3)) || ((card1 == card2) && (card2 == card4)) || ((card1 == card2) && (card2 == card5)) || ((card1 == card3) && (card3 == card4)) 
               || ((card1 == card3) && (card3 == card5)) || ((card1 == card4) && (card4 == card5)) || ((card2 == card3) && (card3 == card4)) || ((card2 == card3) && (card3 ==
              card5)) || ((card2 == card4) && (card4 == card5)) || ((card3 == card4) && (card4 == card5))){
        threeOfAKind = threeOfAKind + 1;
      }
      //if there are two pairs of cards with the same face increment two pair
      if(((card1 == card2) && (card3 == card4)) || ((card1 == card2) && (card3 == card5)) || ((card1 == card2) && (card4 == card5))
                || ((card1 == card3) && (card2 == card4)) || ((card1 == card3) && (card2 == card5)) || ((card1 == card3) && (card4 ==
              card5)) || ((card1 == card4) && (card2 == card3)) || ((card1 == card4) && (card2 == card5)) || ((card1 == card4) && (card3
                == card5)) || ((card1 == card5) && (card2 == card3)) || ((card1 == card5) && (card2 == card4)) || ((card1 == card5) &&
              (card3 == card4))){
        twoPair = twoPair + 1;
      }
      //if there are two cards with the same face value increment one pair
      if((card1 == card2) || (card1 == card3) || (card1 == card4) || (card1 == card5) || (card2 == card3) || (card2 == card4) 
                || (card2 == card5) || (card3 == card4) || (card3 == card5) || (card4 == card5)){
        onePair = onePair + 1;
      }
      //increment the number of loops and hands each run 
      loopCount = loopCount + 1;
      numHands = numHands - 1;
      }while(numHands != 0);
    
      double probFourOfAKind = (double)fourOfAKind / loopCount; //initialize the probability of four of a kind
      double probThreeOfAKind = (double)threeOfAKind / loopCount; //initialize the probability of three of a kind
      double probTwoPair = (double)twoPair / loopCount; //initialize the probability of two pairs
      double probOnePair = (double)onePair / loopCount; //initialize the probability of one pair
      
      System.out.println("The number of loops: " + loopCount); //print the number of loops
      System.out.println("The probability of four of a kind: " + probFourOfAKind); //print the probability of four of a kind
      System.out.println("The probability of three of a kind: " + probThreeOfAKind); //print the probability of three of a kind
      System.out.println("The probability of two pairs: " + probTwoPair); //print the probability of two pairs
      System.out.println("The probability of one pair: " + probOnePair); // print the probability of one pair
      
  }
}


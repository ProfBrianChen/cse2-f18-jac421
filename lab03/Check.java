// Name: Jenna Catalano 
// Date: 9/7/2018
// Course: CSE 002 Section 110
//
// This program will ask the user to input the price of the check, the percentage tip they want to pay,
// and the amount of ways they need the check split in the scenario of going to dinner with friends.
//
import java.util.Scanner;

//initialize public class "Check"
public class Check{
    	// main method required for every Java program
   	  public static void main(String[] args) {
        
        Scanner myScanner = new Scanner( System.in ); //initialize Scanner "myScanner"
          
        System.out.print("Enter the original cost of the check in the form xx.xx: "); //print a statement to ask the user for original cost
        double checkCost = myScanner.nextDouble(); //check cost is the double value inputted by user
        
        System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): "); //print a statement to ask the user what percentage tip
        double tipPercent = myScanner.nextDouble(); //tip percent is the next double value inputted by user
        tipPercent /= 100;//We want to convert the percentage into a decimal value
           
        System.out.print("Enter the number of people who went out to dinner: "); //print a statement to ask the user how many people went to dinner
        int numPeople = myScanner.nextInt(); //the number of people is the next integer value inputted by user
        
        double totalCost;
        double costPerPerson;
        int dollars,   //whole dollar amount of cost 
             dimes, pennies; //for storing digits
             //to the right of the decimal point 
             //for the cost$ 
       totalCost = checkCost * (1 + tipPercent);
       costPerPerson = totalCost / numPeople;
       //get the whole amount, dropping decimal fraction
       dollars=(int)costPerPerson;
       //get dimes amount, e.g., 
       // (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
       //  where the % (mod) operator returns the remainder
       //  after the division:   583%100 -> 83, 27%5 -> 2 
       dimes=(int)(costPerPerson * 10) % 10;
       pennies=(int)(costPerPerson * 100) % 10;
       System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies ); //print the amount each person owes


}  //end of main method   
  	} //end of class


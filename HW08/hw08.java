// Name: Jenna Catalano 
// Date: 11/15/2018
// Course: CSE 002 Section 110
//
// Generates an array representing a deck of cards and shuffles the deck and pulls hands with a specific "numCards"
//
import java.util.Scanner;
import java.util.Arrays;
public class hw08{ 
public static void main(String[] args) { 
Scanner scan = new Scanner(System.in); 
 //suits club, heart, spade or diamond 
 String[] suitNames={"C","H","S","D"};    
String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
String[] cards = new String[52]; 
String[] hand = new String[5]; 
int numCards = 5; 
int again = 1; 
int index = 51;
for (int i=0; i<52; i++){ 
  cards[i]=rankNames[i%13]+suitNames[i/13]; 
  System.out.print(cards[i]+" "); 
} 
System.out.println();
  System.out.println("Your deck is: ");
printArray(cards); 
  System.out.println("Your shuffled deck is: ");
shuffle(cards); 
printArray(cards); 
//if you want another hand print "1" and it will give you another hand
while(again == 1){ 
  //if your index is less than your amount of cards it will generate a new deck and set index back at 51
    if (index < numCards){
     cards = new String[52]; 
    for (int i=0; i<52; i++){ 
      cards[i]=rankNames[i%13]+suitNames[i/13]; 
    }
    shuffle(cards);
    index = 51;
  }
  System.out.println("Your hand is: ");
  hand = getHand(cards,index,numCards); 
  printArray(hand);
  index = index - numCards;
  System.out.println("Enter a 1 if you want another hand drawn"); 
  again = scan.nextInt(); 
} 
}
//prints each value in the array inputted into the method plus a space
public static void printArray(String [] list){
  for(int i=0; i < list.length; i++){
    System.out.print((list[i]) + " ");
  }
  System.out.println();//new line
  return;
}
//switches the first card with a random card 100 times
public static void shuffle(String [] list){
  for(int i=0; i < 100; i++){
    String n = list[0];
    int l = ((int)(Math.random() * (list.length - 1)) + 1);
    String m = list[l];
    list[l] = n;
    list[0] = m;
  }
}
//generates a hand with a given number of cards from the top of the deck (51) and down incrementing
public static String [] getHand(String [] list, int index, int numCards){
  String[] hand = new String[numCards];
  for(int i=0 ; i < numCards; i++){
    String n = list[index-i];
    hand[i] = n;
  }
  return hand;
}

      
}


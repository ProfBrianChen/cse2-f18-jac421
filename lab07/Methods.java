// Name: Jenna Catalano 
// Date: 10/26/2018
// Course: CSE 002 Section 110"
//
// Generates a story based on user inputs.
//
  import java.util.Scanner;
  import java.util.Random;
  //initialize the public class as lab07
  public class Methods{
  
  //main method used for all java 
  public static void main (String[]args){
    paragraph(); //this is only used in phase to to create the paragraph

    //Phase 1 (to run phase 1, uncomment and comment the code added in phase 2
  /*  Scanner scnr = new Scanner(System.in);
    int ans = 1;
    while (ans == 1){ 
      System.out.println("The " + adjective() + " " + subject() + " was " + verb() + " the " + adjective() + " " + 
                         object() + " .");
      System.out.println("Enter 1 if you'd like another sentence, if not, enter 0.");
      ans = scnr.nextIn();
    }*/
  }
  
  //the following four methods are phase 2
  
  //this method creates a paragraph from randomly generated sentences
  public static void paragraph(){
   String a = adjective();
   String s = subject();
   String v = verb();
   String o = object();
   String a1 = adjective();
   int increment = 0; // create an increment for the while loop
   Random randomGenerator = new Random();
   int randomInt = randomGenerator.nextInt(10);//only generates up to 10 random sentences to avoid too long paragraphs
   sentence1(a, a1, s, v, o);//calls sentence one with previously called word types
   //generates random amount of action sentences 1-10
   while (increment <= randomInt){
   sentence2(s);
   increment++;
   }
   conclusion(s);//calls conclusion
  }
  //creates a random thesis from types of words
  public static void sentence1(String v, String w , String x, String y, String z){
    System.out.println ("The " + w + " " + x + " was " + y + " the " + v + " " + z + " .");
  }
  //creates a random action statement but with the same subject as sentence one or the word "it" randomly
  public static void sentence2(String b){
   Random randomGenerator = new Random();
   int randomInt = randomGenerator.nextInt(2);
   if (randomInt == 0){
    System.out.println ("The " + b + " used a " + object() + " for " + verb() + " the " + object() + " around the " + adjective()+ " " 
                          + object() + " .");
  }
   else{
     System.out.println("It used a " + object() + " for " + verb() + " the " + object() + " around the " + adjective()+ " " 
                          + object() + " .");
   }
  }
  //creates a conclusion with same subject as other sentences
  public static void conclusion(String c){
    System.out.println ("That " + c + " was " + verb() + " her " + object() + " !");
  }
  //end of phase 2
  
  //randomly assigns an adjective choice from 10 options
    public static String adjective(){
      Random randomGenerator = new Random();
      int randomInt = randomGenerator.nextInt(10);
      String adj = "no";
      switch(randomInt){
        case 0:
         adj = "pretty";
          break;
        case 1:
          adj = "spikey";
          break;
        case 2:
         adj = "seafoam green";
          break;
        case 3:
          adj = "painful";
          break;
        case 4:
          adj = "ugly";
          break;
        case 5:
         adj = "round";
          break;
        case 6:
          adj = "tall";
          break;
        case 7 :
          adj = "fat";
          break;
        case 8:
          adj = "mustard yellow";
          break;
        case 9:
          adj = "tiny";
          break;
      }
      return adj;
    }
     //randomly assigns a subject choice from 10 options
       public static String subject(){
          Random randomGenerator = new Random();
      int randomInt = randomGenerator.nextInt(10);
      String sub = "no";
   
      switch(randomInt){
        case 0:
          sub = "penguin";
          break;
        case 1:
         sub = "shark";
          break;
        case 2:
          sub = "seahorse";
          break;
        case 3:
         sub = "princess";
          break;
        case 4:
         sub = "unicorn";
          break;
        case 5:
          sub = "raccoon";
          break;
        case 6:
          sub = "tourist";
          break;
        case 7 :
          sub = "father";
          break;
        case 8:
          sub = "mother";
          break;
        case 9:
          sub = "tortoise";
          break;
      }
      return sub;
    }
       //randomly assigns an object choice from 10 options
         public static String object(){
            Random randomGenerator = new Random();
      int randomInt = randomGenerator.nextInt(10);
      String obj = "no";
      switch(randomInt){
        case 0:
          obj = "pinecone";
          break;
        case 1:
          obj = "spike";
          break;
        case 2:
         obj = "star";
          break;
        case 3:
          obj = "prize";
          break;
        case 4:
          obj = "umbrella";
          break;
        case 5:
          obj = "peanut";
          break;
        case 6:
          obj = "tub";
          break;
        case 7 :
          obj = "snack";
          break;
        case 8:
          obj = "mango";
          break;
        case 9:
          obj = "tree";
          break;
      }
      return obj;
    }
         //randomly assigns a verb choice from 10 options
          public static String verb(){
       Random randomGenerator = new Random();
      int randomInt = randomGenerator.nextInt(10);
      String v = "no";
      switch(randomInt){
        case 0:
          v = "picking up";
          break;
        case 1:
          v = "starting";
          break;
        case 2:
          v = "saving";
          break;
        case 3:
          v = "plucking";
          break;
        case 4:
          v = "undressing";
          break;
        case 5:
          v = "rubbing";
          break;
        case 6:
          v = "tasting";
          break;
        case 7 :
          v = "favoring";
          break;
        case 8:
          v = "making";
          break;
        case 9:
          v = "touching";
          break;
      }
      return v;
    }
  }
  
          
       
               
//Jenna Catalano
//9/18/2018
//Section: 110
//CSE 02 Convert
//
//This program will convert the quantity of rain from a hurricane given by the user in acres and inches into cubic miles.
//
import java.util.Scanner;

//initialize public class Convert
public class Convert{
  //main method required for every java program
  public static void main (String[]args){
    
    Scanner myScanner = new Scanner(System.in); //intialize Scanner myScanner
    
    System.out.print("Enter the affected area in acres: "); //print out a statement asking the user for number of acres
    double acres = myScanner.nextDouble(); //the amount of acres is the double value inputted by the user
    
    System.out.print("Enter the rainfall in the affected area: "); //print out a statement asking the user for inches of rainfall in the affected area
    double rainfall = myScanner.nextInt(); //the inches of rainfall is the integer value inputted by the user
    
    double rainQuantity = ((rainfall * acres) * 27154) / 1101117147428.57; //converted the rain quantity in acre inches to gallons and then to cubic miles
    
    System.out.println(rainQuantity + " cubic miles"); //print the amount of cubic miles
    
  }
}
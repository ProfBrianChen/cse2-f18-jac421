//Jenna Catalano
//9/18/2018
//Section: 110
//CSE 02 Pyramid
//
//This program will find the volume of a pyramid when given the dimensions.
//
import java.util.Scanner;

  //intialize public class Pyramid
public class Pyramid{
  //main method required for every java program
  public static void main (String[]args){
    
    Scanner myScanner = new Scanner(System.in); //initialize Scanner "myScanner"
    
    System.out.print("The square side of the pyramid is (input length): "); //print a statement asking the user for the length of the square side of a pyramid
    int length = myScanner.nextInt(); //the length of the square side of the pyramid is the next integer value inputted by the user
    
    System.out.print("The height of the pyramid is (input height): "); //print a statement asking the user for the height of the pyramid
    int height = myScanner.nextInt(); //the height of the pyramid is the next integer value inputted by the user
    
    int volume = ((length * length) * height) / 3; //find the volume of the pyramid given its length and height
    System.out.println("The volume inside the pyramid is: " + volume); //print a statement telling the user the volume of the pyramid
    
  }
}

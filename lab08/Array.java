// Name: Jenna Catalano 
// Date: 11/9/2018
// Course: CSE 002 Section 110
//
// Creates an array of 100 integers 1-100 and counts the occurences of each integer
//
//initialize the public class as Arrays
import java.util.Arrays;
public class Array{
  //main method used for all java code
  public static void main(String[]args){
    //create two arrays of length 100
    int [] array1 = new int[100];
    int [] array2 = new int[100];
    //fill array1 with integers 0-99
    for(int i = 0; i < array1.length ; i++){
      array1[i] = (int)(Math.random() * 99 + 1);
    }
   System.out.println("The values in Array one are " + Arrays.toString(array1));//print array 1
   //count the amount of occurences of each number in array one
   for(int i = 0; i <= 99; i++){
     int count = 0;
     for(int j= 0; j < array1.length ; j++){
       if(i == array1[j]){
       count++;
     }
     }
     System.out.println("The number of occurances of " + i + " is " + count);//print the occurence of each number
     array2[i] = count;//make the array 2 values equal to the amount of each number in array one in order 0-99
   }
   //prints the values in array 2 which are the amount of each value in array 1
   System.out.println("The amounts of each number 0-99 in array one are " + Arrays.toString(array2));
  }
}

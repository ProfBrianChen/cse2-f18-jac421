// Name: Jenna Catalano 
// Date: 10/23/2018
// Course: CSE 002 Section 110
//
// Based on the user inputted value this code will generate a certain grid size of an encrypted x pattern.
//
import java.util.Scanner;
//initialize the public class as EncryptedX
public class EncryptedX{
  //main method used for all java code
  public static void main(String[]args){
    
    Scanner scnr = new Scanner(System.in); //initialize the scanner scnr
    int input = -1; //initialize the integer input as -1
    
    /* the do while loop asks a user for an input and determines if it is a string or if it is not in between zero and 
     one hundred, if either of these things are true it generates an error message, if not it accepts the input */
    do{
      System.out.println("Enter an integer value (1-100) for grid size.");
      boolean correctInt = scnr.hasNextInt();
      if(correctInt){
        input = scnr.nextInt();
        if(input < 0 || input > 100){
          System.out.println("That was not a number from one to ten");
        }
      }
      else{
        System.out.println("You did not enter a valid number for grid size.");
        scnr.next();
      }
    }while(input < 0 || input > 100);
    
    //creates an encrypted x pattern for a given grid size which is the input
    for (int row = 0; row<=input; row++)  
        {  
            for (int col=0; col<=input; col++)  
            {  
              // will print spaces in an x pattern
                if (row == col || col == (input - row))  
                  System.out.print(" ");  
               //will print stars so that you can see the hidden x's
                else  
                  System.out.print("*");   

            }
             System.out.println(); 
    }
  }
}
 
// Name: Jenna Catalano 
// Date: 10/30/2018
// Course: CSE 002 Section 110
//
// This program will take a user text input and modify or analyze the contents of it based on their choice from the 
// printed menu.
//
import java.util.Scanner;

//initialize the public class as hw05
public class WordTools{
  
  //main method used for all java code
  public static void main (String[]args){
    Scanner scnr = new Scanner(System.in);//initialize Scanner scnr
    String text = sampletext();//retrieve user input from method sampletext
    System.out.println("You entered: " + text);//output the user inputted text
    String choice; //initialize String choice
    
    do{
      printMenu();//call method printMenu
      choice = scnr.next();//retrieve user choice from menu options
      //if they don't print one of their choices, tell them that thats wrong
      if (!((choice.equals("c")) || (choice.equals("w")) || (choice.equals("f")) || (choice.equals("r")) || 
            (choice.equals("s")) || (choice.equals("q")))){
        System.out.println("Error: invalid input");
      }
      else{
        //based on the user choice, choose the proper corresponding method and output the result
        switch (choice){
          case "c":
            System.out.println("The number of non-whitespace characters is " + getNumOfNonWSCharacters(text));
            break;
          case "w":
            System.out.println("The number of words is " + getNumOfWords(text));
            break;
          case "f":
            System.out.println("Enter a word or phrase to be found");
            String word = scnr.next();//gets a word from the user to count in the text
            System.out.println("The number of " + word + " instances is " + findText(word, text));
            break;
          case "r":
            System.out.println("Edited String: " + replaceExclamation(text));
            break;
          case "s":
            System.out.println("Edited String: " + shortenSpace(text));
            break;
           //if the user chooses quit, then no longer print the menu and end
          case "q":
            break;
        }
      }   
    }while(!(choice.equals("q"))); //if the user chooses quit, then no longer print the menu and end
  }
  
  //counts the amount of non-whitespace characters and returns the amount 
  public static int getNumOfNonWSCharacters(String x){
    int numc = 0;
    for(int i = 0; (i < (x.length())) ; i++){
      char a = x.charAt(i);
      if (a != ' '){
        numc++;
      }
    }
    return numc;
  }
  
  //counts the number of words in the text and returns the amount
  public static int getNumOfWords(String x){
    int numw = 1;
    String y = shortenSpace(x);
    for(int i = 0; (i < (y.length())); i++){
      char a = y.charAt(i);
      if (a == ' '){
        numw++;
      }
    }
    return numw;
  }
  
  //counts the amount of times a user inputted word is found in the text and returns the amount
  public static int findText(String y, String x){
    int diff = (x.length()) - (x.replaceAll(y ,"").length());
    int numf = (int)diff / (int)(y.length());
    return numf;
  }
  
  //replaces exclamation marks in the text with periods and returns the updated string
  public static String replaceExclamation(String x){
    String textr = x.replaceAll("!",".");
    return textr;
  }
  
  //finds spots in the text with two or more spaces in a row and replaces them with a single space
  public static String shortenSpace(String x){
    String texts = x;
    while(texts.contains("  ")){
      texts = texts.replaceAll("  " , " ");
    }
    return texts;
  }
  
  //obtains a sample text from the user
  public static String sampletext(){
    Scanner scnr = new Scanner(System.in);
    System.out.println("Enter a sample text: ");
    String text = scnr.nextLine();
    return text;
  }
  
  //prints a menu of different text modification options
  public static void printMenu(){
    System.out.println("MENU");
    System.out.println();
    System.out.println("c - Number of non-whitespace characters");
    System.out.println("w - Number of words");
    System.out.println("f - Find text");
    System.out.println("r - Replace all !'s");
    System.out.println("s - Shorten spaces");
    System.out.println("q - Quit");
    System.out.println();
    System.out.println("Choose an option:");
  }
}

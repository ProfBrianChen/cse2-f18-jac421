//Jenna Catalano
//9/25/2018
//Section: 110
//CSE 02 CrapsSwitch
//
//This program will the slang term for a roll from two dice that is either random or selected using switch statements
//

import java.util.Scanner;

//initialize public class "CrapsSwitch"
public class CrapsSwitch{
//main method required for every java program
public static void main (String[]args){
  
  Scanner scnr = new Scanner(System.in); //initialize Scanner scnr
  int diceResult1 = 0; //initialize diceResult1 as 0
  int diceResult2 = 0; //initialize diceResult2 as 0
  String slang = "none"; // intialize slang as none
  
  System.out.println("To state the two dice, type 'state', To randomly cast the dice, type 'random' ");  //print a statement asking the user to choose state or random
  String choice = scnr.nextLine(); //get a string from the user
  
  switch(choice){
    case "state":  //if the user prints state, have them print the numbers of each dice
      System.out.println("Choose a number 1-6");
      diceResult1 = scnr.nextInt();
    
      System.out.println("Choose another number 1-6");
      diceResult2 = scnr.nextInt();
      break;
    case "random": //if the user prints random, choose two random dice numbers
      diceResult1 = (int) (Math.random() * 6) + 1;
      System.out.println("The first cast is " + diceResult1);
    
      diceResult2 = (int) (Math.random() * 6) + 1;
      System.out.println("The second cast is " + diceResult2);
      break;
    default: //if the user picks neither of the choices tell them they are wrong
       System.out.println("Error, user didn't enter one of the two possible inputs.");
      break;
  }
  
  //based on the values of diceResult1 and diceResult2 print the proper slang statement for this roll, if the values aren't 1-6, print error
  switch(diceResult1){
    case 1:
      switch(diceResult2){
        case 1:
          slang = "Snake Eyes";
          break; 
        case 2:
          slang = "Ace Deuce";
            break;
        case 3:
          slang = "Easy Four";
          break;
        case 4:
          slang = "Fever Five";
            break;
        case 5:
          slang = "Easy Six";
          break;
        case 6:
          slang = "Seven out";
            break; 
        default:
          System.out.println("Error");
          break;
      }
          break;
      
    case 2:
       switch(diceResult2){
        case 1:
          slang = "Ace Deuce";
          break;
        case 2:
          slang = "Hard Four";
            break;
        case 3:
          slang = "Fever Five";
          break;
        case 4:
          slang = "Easy Six";
            break;
        case 5:
          slang = "Seven out";
          break;
        case 6:
          slang = "Easy Eight";
            break; 
         default:
          System.out.println("Error");
          break;
       }
          break;
       
      case 3:
       switch(diceResult2){
        case 1:
          slang = "Easy Four";
          break;
        case 2:
          slang = "Fever Five";
            break;
        case 3:
          slang = "Hard Six";
          break;
        case 4:
          slang = "Seven out";
            break;
        case 5:
          slang = "Easy Eight";
          break;
        case 6:
          slang = "Nine";
            break; 
         default:
          System.out.println("Error");
          break;
       }
          break;
       
      case 4:
       switch(diceResult2){
        case 1:
          slang = "Fever Five";
          break;
        case 2:
          slang = "Easy Six";
            break;
        case 3:
          slang = "Seven out";
          break;
        case 4:
          slang = "Hard Eight";
            break;
        case 5:
          slang = "Nine";
          break;
        case 6:
          slang = "Easy Ten";
            break; 
         default:
          System.out.println("Error");
          break;
       }
          break;
       
      case 5:
       switch(diceResult2){
        case 1:
          slang = "Easy Six";
          break;
        case 2:
          slang = "Seven out";
            break;
        case 3:
          slang = "Easy Eight";
          break;
        case 4:
          slang = "Nine";
            break;
        case 5:
          slang = "Hard Ten";
          break;
        case 6:
          slang = "Yo-leven";
            break; 
         default:
          System.out.println("Error");
          break;
       }
          break;
       
    case 6:
       switch(diceResult2){
        case 1:
          slang = "Seven out";
          break;
        case 2:
          slang = "Easy Eight";
            break;
        case 3:
          slang = "Nine";
          break;
        case 4:
          slang = "Easy Ten";
            break;
        case 5:
          slang = "Yo-leven";
          break;
        case 6:
          slang = "Boxcars";
            break; 
         default:
          System.out.println("Error");
          break;
       }
          break;
  }
  System.out.println("The slang term for this roll is " + slang); //print a statement telling the user the proper slang statement for their roll
}
}

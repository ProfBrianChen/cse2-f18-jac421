//Jenna Catalano
//9/25/2018
//Section: 110
//CSE 02 CrapsIf
//
//This program will determine the slang term for a roll from two dice that is either random or selected using If statements
//

import java.util.Scanner;

//initialize public class "CrapsIf"
public class CrapsIf{
//main method required for every java program
public static void main (String[]args){
  
  Scanner scnr = new Scanner(System.in); //initialize Scanner scnr
  int diceResult1 = 0; //initialize diceResult1 as 0
  int diceResult2 = 0; //initialize diceResult2 as 0
  String slang = "none"; //initialize slang as none
  
  System.out.println("To state the two dice, type 'state', To randomly cast the dice, type 'random' "); //print a statement asking the user to choose state or random
  String choice = scnr.nextLine(); //get a string from the user
  
  //if the user prints state, have them print the numbers of each dice
  if(choice.equals("state")){
    System.out.println("Choose a number 1-6");
    diceResult1 = scnr.nextInt();
    
    System.out.println("Choose another number 1-6");
    diceResult2 = scnr.nextInt();
  }
  //if the user prints random, choose two random dice numbers
  else if(choice.equals("random")){
    diceResult1 = (int) (Math.random() * 6) + 1;
    System.out.println("The first cast is " + diceResult1);
    
    diceResult2 = (int) (Math.random() * 6) + 1;
    System.out.println("The second cast is " + diceResult2);
  }
  //if the user picks neither of the choices tell them they are wrong
  else{
    System.out.println("Error, user didn't enter one of the two possible inputs.");
  }
  
  //based on the values of diceResult1 and diceResult2 print the proper slang statement for this roll, if the values aren't 1-6, print error
  if (diceResult1 == 1 && diceResult2 == 1){
     slang = "Snake Eyes";
   }
  else if (diceResult1 == 1 && diceResult2 == 2 || diceResult1 == 2 && diceResult2 == 1){
    slang = "Ace Deuce";
  }
  else if (diceResult1 == 2 && diceResult2 == 2){
    slang = "Hard Four";
  }
  else if (diceResult1 == 3 && diceResult2 == 1 || diceResult1 == 1 && diceResult2 == 3){
    slang = "Easy Four";
  }
  else if (diceResult1 == 3 && diceResult2 == 2 || diceResult1 == 2 && diceResult2 == 3 || diceResult1 == 1 && diceResult2 == 4 || diceResult1 == 4 && diceResult2 == 1){
    slang = "Fever Five";
  }
  else if (diceResult1 == 3 && diceResult2 == 3){
    slang = "Hard Six";
  }
  else if (diceResult1 == 1 && diceResult2 == 5 || diceResult1 == 5 && diceResult2 == 1 || diceResult1 == 2 && diceResult2 == 4 || diceResult1 == 4 && diceResult2 == 2){
    slang = "Easy Six";
  }
  else if (diceResult1 == 1 && diceResult2 == 6 || diceResult1 == 1 && diceResult2 == 6 || diceResult1 == 2 && diceResult2 == 5 || diceResult1 == 5 && diceResult2 == 2 || 
           diceResult1 == 4 && diceResult2 == 3 || diceResult1 == 3 && diceResult2 == 4){
    slang = "Seven out";
  }
  else if (diceResult1 == 2 && diceResult2 == 6 || diceResult1 == 6 && diceResult2 == 2 || diceResult1 == 3 && diceResult2 == 5 || diceResult1 == 5 && diceResult2 == 3){
    slang = "Easy Eight";
  }
  else if (diceResult1 == 4 && diceResult2 == 4){
    slang = "Hard Eight";
  }
  else if (diceResult1 == 4 && diceResult2 == 5 || diceResult1 == 5 && diceResult2 == 4 || diceResult1 == 3 && diceResult2 == 6 || diceResult1 == 6 && diceResult2 == 3){
    slang = "Nine";
  }
  else if (diceResult1 == 6 && diceResult2 == 4 || diceResult1 == 4 && diceResult2 == 6){
    slang = "Easy Ten";
  }
  else if (diceResult1 == 5 && diceResult2 == 5){
    slang = "Hard Ten";
  }
  else if (diceResult1 == 6 && diceResult2 == 5 || diceResult1 == 5 && diceResult2 == 6){
    slang = "Yo-leven";
  }
  else if (diceResult1 == 6 && diceResult2 == 6){
    slang = "Boxcars";
  }
  else{
    System.out.println("Error");
  }
  
  System.out.println("The slang term for this roll is " + slang); //print a statement telling the user the proper slang statement for their roll
}
}
// Name: Jenna Catalano 
// Date: 9/7/2018
// Course: CSE 002 Section 110
//
// This program will take  the recorded rotations and amount of seconds per two bike trips recorded on a bike cyclometer and print the amount of minutes per trip, 
// roation count per trip, distance in miles per trip, and overall distance in miles for the two trips together.
//
public class Cyclometer{
  //main method required for every Java program
  public static void main(String[] args) {

    int secsTrip1   = 480;   // the amount of seconds recorded for trip 1
   	int secsTrip2   = 3220;  // the amount of seconds recorded for trip 2
  	int countsTrip1 = 1561;  // the amount of rotations recorded for trip 1
  	int countsTrip2 = 9037;  // the amount of rotations recorded for trip 2
    
    double wheelDiameter = 27.0,  // the diameter of the wheel is 27.0 inches
  	PI = 3.14159,                 // the value of pi is 3.14159
  	feetPerMile = 5280,           // there are 5280 feet in one mile
  	inchesPerFoot = 12,           // there are 12 inches in a foot
  	secondsPerMinute = 60;        // there are 60 seconds in a minute
    
	  double distanceTrip1, distanceTrip2,totalDistance;  // initializes the distance in miles for trip one, the distance in miles for trip 2 and the distance in miles for both.
    
    // prints the numbers stored in variables that are in the unit "seconds", converted to minutes, and the amount of rotations
    System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts.");
	  System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts.");

    distanceTrip1  = countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	  distanceTrip1 /= inchesPerFoot*feetPerMile;                              // Gives distance in miles for trip 1
	  distanceTrip2  = countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; // Calculates distance in miles for trip 2 from rotations of the wheel times diameter pi 
	  totalDistance  = distanceTrip1+distanceTrip2;                            // Calculates total distance in miles for both trips together
    
    //Print out the output data.
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
	  System.out.println("Trip 2 was "+distanceTrip2+" miles");
	  System.out.println("The total distance was "+totalDistance+" miles");
 

	



    
  } //end of main method
} //end of class
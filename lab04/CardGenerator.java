// Name: Jenna Catalano 
// Date: 9/21/2018
// CardGenerator CSE 002 Section 110
//
// This program will pick a random number of card and assign it a suit and identity.
//
//initialize public class "CardGenerator"
public class CardGenerator{
    	// main method required for every Java program
   	  public static void main(String[] args) {
        
        int cardNumber = (int)(Math.random() * ((52 - 1) + 1)) + 1; //picks a random card number (1-52)
        
        String cardSuit = "Suit"; //intitalizes string "cardSuit"
        String cardIdentity = "Identity"; //initiallizes string "cardIdentity"
        
        if (cardNumber <= 13 && cardNumber >= 1){
          cardSuit = "Diamonds";}
            else if (cardNumber <= 26 && cardNumber >= 14 ){
              cardSuit = "Clubs";}
                else if (cardNumber <= 39 && cardNumber >= 27){
                  cardSuit = "Hearts";}
                  else if (cardNumber <= 52 && cardNumber >= 40){
                    cardSuit = "Spades";}
        
          int remainder = cardNumber % 13;
        
        switch(remainder){
          case 0:
            cardIdentity = "King";
            break;
          case 1:
            cardIdentity = "Queen";
            break;
          case 2:
            cardIdentity = "Jack";
            break;
          case 3:
            cardIdentity = "10";
            break;
          case 4:
            cardIdentity = "9";
            break;
          case 5:
            cardIdentity = "8";
            break;
          case 6:
            cardIdentity = "7";
            break;
          case 7:
            cardIdentity = "6";
            break;
          case 8:
            cardIdentity = "5";
            break;
          case 9:
            cardIdentity = "4";
            break;
          case 10:
            cardIdentity = "3";
            break;
          case 11:
            cardIdentity = "2";
            break;
          case 12:
            cardIdentity = "Ace";
            break;
     
        }
        
          System.out.println("You picked the " + cardIdentity + " of " + cardSuit);
            }
        }
        
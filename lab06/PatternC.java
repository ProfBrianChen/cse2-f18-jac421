// Name: Jenna Catalano 
// Date: 10/9/2018
// Course: CSE 002 Section 110
//
// Based on the user inputted value this code will generate a certain number of rows of Pattern C pyramid.
//
import java.util.Scanner;
//initialize the public class as PatternC
public class PatternC{
  //main method used for all java code
  public static void main(String[]args){
    
    Scanner scnr = new Scanner(System.in); //initialize the scanner scnr
    int input = 0; //initialize the integer input as 0
    
    /* the do while loop asks a user for an input and determines if it is a string or if it is not in between one and 
     ten, if either of these things are true it generates an appropriate error message, if not it accepts the input */
    do{
      System.out.println("Enter an integer value (1-10) for number of rows.");
      boolean correctInt = scnr.hasNextInt();
      if(correctInt){
        input = scnr.nextInt();
        if(input < 1 || input > 10){
          System.out.println("That was not a number from one to ten");
        }
      }
      else{
        System.out.println("You did not enter a valid number of rows.");
        scnr.next();
      }
    }while(input < 1 || input > 10);
    
    for(int numRows = 1 ; numRows <= input; ++numRows){ //determines how many rows to print based on input
      for(int count = 10; count > numRows ; count-- ){ //determines how many spaces to start each row
        System.out.print(" "); //prints space
      }
      for(int number = numRows ; number >= 1 ; --number){ //which order and how many numbers per row
        System.out.print( number ); //prints the appropriate number
      }
      System.out.println(); //the print statement that generates the next row
    }
  }
}
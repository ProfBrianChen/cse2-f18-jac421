/////////////
//// CSE 02 Welcome Class
///
public class WelcomeClass{
  
  public static void main(String args[]){
    
    //prints Welcome to the class with ID and autobiography to the terminal window
    System.out.println("   -----------");
    System.out.println("   | WELCOME |");
    System.out.println("   -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-J--A--C--4--2--1->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
    System.out.println("My name is Jenna Catalano and I am a sophomore Chemical Engineering student."); 
    System.out.println("I'm orginally from Howard, Pa which is about a half an hour from Penn State main campus.");
    System.out.println("On campus, I'm involved with choir, SWE, Alpha Omega Epsilon, and research with the CHE department.");
    System.out.println("This semester I will also be an Apprentice Teacher in the lab portion of Engineering 5.");
    System.out.println("I love Lehigh and I'm looking forward to another great year. Go Mountain Hawks!");
    
  }
}
   